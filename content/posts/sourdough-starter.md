---
title: "Sourdough Starter"
slug: sourdough-starter-recipe
date: 2020-05-10T18:02:23-07:00
description: how to make, store and preserve sourdough starter
tags:
- bread
---

# Ingredients
- 50g whole wheat OR rye flour
- 50g unbleached white flour
- 100g filtered water (room temp)

# To start

1. Mix flour and water in a small bowl or tupperware/locking lid container
2. Let stand in a warm location in your kitchen for 24-48 hours or until you observe bubbling.  It is normal for a new starter to rise vigorously when mixed and then stop rising or bubbling at all
3. Every day, take two tablespoons of starter mixture and add it to a new preparation of flour and water
4. When the starter rises and falls consistently, doubling in size every time it is ready to bake with

_For best results, use your sourdough starter to bake with when it has risen to its peak before beginning to fall again (5-6 hours at room temp)._

# Maintainence

- When not in use, starter can be stored for up to a week in the fridge without feeding
- Feed refridgerated starter every 3-4 days
- Check starter regularly to make sure it isn't going moldy
- Discard any accumulated dark or black liquids that appear on the surface of the starter -- this is normal and expected
- Make sure to resume daily feedings at room temperature at least 48 hours before baking

# Preservation

_Preserve a sourdough starter in this way if you anticipate being unable to feed it for longer than 2 weeks._

1. When your starter has risen to its peak, spread thinly on to parchment or plastic wrap
2. Allow to dry over 48 hours
3. Break up sourdough flakes and store in a cool, dry place (keeps for 2-3 months, freeze for ∞ shelf-life)

# Notes

- Sourdough will evolve and develop over time, especially if it is fed and kept at room temperature
- Place a napkin or paper towel under your starter's container if keeping at room temperature -- it will probably overflow!
- You can keep a smaller portion of starter to save money on feedings -- the important thing is that there is equal parts flour to water, so you could use 40 grams of flour and water instead of 100g.  Make sure to use plenty of old starter every time you feed though.  Smaller containers are bad for the starter's "immune system"