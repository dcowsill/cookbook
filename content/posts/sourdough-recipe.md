---
title: "Sourdough Recipe"
slug: sourdough-recipe
date: 2020-05-10T14:47:57-07:00
description: for your quarantine carb cravings
tags:
- bread
---

Makes one loaf.  Double recipe as needed for larger batches

# Ingredients
- 350g water
- 100g active sourdough starter
- 450g all-purpose white flour (unbleached)
- 50g whole wheat flour
- 20g salt

# Directions
1. Combine water and flour together in a large mixing bowl and mix to a loose, shaggy mass of shitty-looking dough.  Let flour/water mix sit for 45 minutes or up to 2 hours ([YouTube](https://www.youtube.com/watch?v=zgz0oAhgwyg))

2. Add sourdough starter and salt to dough and squeeze the dough together to combine.  Use wet hands because it's a really wet dough.  Cover and let stand for 10 minutes ([YouTube](https://youtu.be/sPzpU1clHaA?t=396))

3. Grab a slab of dough with a wet hand and fold it over the rest.  Continue folding three or four times.  Repeat this process every 10 minutes 4 or 5 times to develop the gluten ([YouTube](https://youtu.be/sPzpU1clHaA?t=471))

4. Allow dough to ferment until it doubles in size.  You can do this over 24-48 hours in a fridge or 4-6 hours at room temp.  Leave the dough in a warm place to speed the process up (oven with the oven light on works well)

5. Turn the dough out onto a well-floured counter.  Form into loose balls (heh) with floured hands or a bench scraper.  Let sit for 10-20 minutes ([YouTube](https://youtu.be/sPzpU1clHaA?t=652))

6. Fold the dough over itself and form a tight ball.  Flour the shit out of it because it likes to stick.  Place in a well-floured bowl with cloth, a collander with cloth, or a purpose made 'banneton' (french basket thing)

7. Stick a cast iron dutch oven in your regular oven and pre-heat to 500 degrees (you heard me.  do it).  Since my oven's a piece of shit it takes about an hour to get to 500 degrees, which is just about long enough for the dough to proof so it kinda works out.  If you have a better oven, pre-heat later

8. Dump out the dough from the basket into the dutch oven and pray the fucker didn't stick

9. Bake covered at 500 degrees for 20 minutes, uncovered for another 20-30 minutes depending on how 'rustic' (shitty but photogenic) you want your bread to be

10. Allow to cool for at least 45 minutes.  Or don't fresh warm bread's fucking dope
